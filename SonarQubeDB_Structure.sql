/*
 Source Server         : Local PostgreSQL
 Source Server Type    : PostgreSQL
 Source Server Version : 110002
 Source Host           : localhost:5432
 Source Catalog        : postgres
 Source Schema         : public

 Target Server Type    : PostgreSQL
 Target Server Version : 110002
 File Encoding         : 65001

 Date: 03/05/2019 11:08:57
*/


-- ----------------------------
-- Table structure for active_rule_parameters
-- ----------------------------
DROP TABLE IF EXISTS "public"."active_rule_parameters";
CREATE TABLE "public"."active_rule_parameters" (
  "id" int4 NOT NULL DEFAULT nextval('active_rule_parameters_id_seq'::regclass),
  "active_rule_id" int4 NOT NULL,
  "rules_parameter_id" int4 NOT NULL,
  "value" varchar(4000) COLLATE "pg_catalog"."default",
  "rules_parameter_key" varchar(128) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for active_rules
-- ----------------------------
DROP TABLE IF EXISTS "public"."active_rules";
CREATE TABLE "public"."active_rules" (
  "id" int4 NOT NULL DEFAULT nextval('active_rules_id_seq'::regclass),
  "profile_id" int4 NOT NULL,
  "rule_id" int4 NOT NULL,
  "failure_level" int4 NOT NULL,
  "inheritance" varchar(10) COLLATE "pg_catalog"."default",
  "created_at" int8,
  "updated_at" int8
)
;

-- ----------------------------
-- Table structure for alm_app_installs
-- ----------------------------
DROP TABLE IF EXISTS "public"."alm_app_installs";
CREATE TABLE "public"."alm_app_installs" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "alm_id" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "owner_id" varchar(4000) COLLATE "pg_catalog"."default" NOT NULL,
  "install_id" varchar(4000) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL,
  "is_owner_user" bool NOT NULL,
  "user_external_id" varchar(255) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for analysis_properties
-- ----------------------------
DROP TABLE IF EXISTS "public"."analysis_properties";
CREATE TABLE "public"."analysis_properties" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "snapshot_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "kee" varchar(512) COLLATE "pg_catalog"."default" NOT NULL,
  "text_value" varchar(4000) COLLATE "pg_catalog"."default",
  "clob_value" text COLLATE "pg_catalog"."default",
  "is_empty" bool NOT NULL,
  "created_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for ce_activity
-- ----------------------------
DROP TABLE IF EXISTS "public"."ce_activity";
CREATE TABLE "public"."ce_activity" (
  "id" int4 NOT NULL DEFAULT nextval('ce_activity_id_seq'::regclass),
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "task_type" varchar(15) COLLATE "pg_catalog"."default" NOT NULL,
  "status" varchar(15) COLLATE "pg_catalog"."default" NOT NULL,
  "submitter_uuid" varchar(255) COLLATE "pg_catalog"."default",
  "submitted_at" int8 NOT NULL,
  "started_at" int8,
  "executed_at" int8,
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL,
  "execution_time_ms" int8,
  "analysis_uuid" varchar(50) COLLATE "pg_catalog"."default",
  "error_message" varchar(1000) COLLATE "pg_catalog"."default",
  "error_stacktrace" text COLLATE "pg_catalog"."default",
  "worker_uuid" varchar(40) COLLATE "pg_catalog"."default",
  "execution_count" int4 NOT NULL,
  "error_type" varchar(20) COLLATE "pg_catalog"."default",
  "main_component_uuid" varchar(40) COLLATE "pg_catalog"."default",
  "component_uuid" varchar(40) COLLATE "pg_catalog"."default",
  "is_last" bool NOT NULL,
  "is_last_key" varchar(55) COLLATE "pg_catalog"."default" NOT NULL,
  "main_is_last" bool NOT NULL,
  "main_is_last_key" varchar(55) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for ce_queue
-- ----------------------------
DROP TABLE IF EXISTS "public"."ce_queue";
CREATE TABLE "public"."ce_queue" (
  "id" int4 NOT NULL DEFAULT nextval('ce_queue_id_seq'::regclass),
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "task_type" varchar(15) COLLATE "pg_catalog"."default" NOT NULL,
  "status" varchar(15) COLLATE "pg_catalog"."default",
  "submitter_uuid" varchar(255) COLLATE "pg_catalog"."default",
  "started_at" int8,
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL,
  "worker_uuid" varchar(40) COLLATE "pg_catalog"."default",
  "execution_count" int4 NOT NULL,
  "main_component_uuid" varchar(40) COLLATE "pg_catalog"."default",
  "component_uuid" varchar(40) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for ce_scanner_context
-- ----------------------------
DROP TABLE IF EXISTS "public"."ce_scanner_context";
CREATE TABLE "public"."ce_scanner_context" (
  "task_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "context_data" bytea NOT NULL,
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for ce_task_characteristics
-- ----------------------------
DROP TABLE IF EXISTS "public"."ce_task_characteristics";
CREATE TABLE "public"."ce_task_characteristics" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "task_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "kee" varchar(512) COLLATE "pg_catalog"."default" NOT NULL,
  "text_value" varchar(512) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for ce_task_input
-- ----------------------------
DROP TABLE IF EXISTS "public"."ce_task_input";
CREATE TABLE "public"."ce_task_input" (
  "task_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "input_data" bytea,
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for ce_task_message
-- ----------------------------
DROP TABLE IF EXISTS "public"."ce_task_message";
CREATE TABLE "public"."ce_task_message" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "task_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "message" varchar(4000) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for default_qprofiles
-- ----------------------------
DROP TABLE IF EXISTS "public"."default_qprofiles";
CREATE TABLE "public"."default_qprofiles" (
  "organization_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "language" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "qprofile_uuid" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for deprecated_rule_keys
-- ----------------------------
DROP TABLE IF EXISTS "public"."deprecated_rule_keys";
CREATE TABLE "public"."deprecated_rule_keys" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "rule_id" int4 NOT NULL,
  "old_repository_key" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "old_rule_key" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for duplications_index
-- ----------------------------
DROP TABLE IF EXISTS "public"."duplications_index";
CREATE TABLE "public"."duplications_index" (
  "id" int8 NOT NULL DEFAULT nextval('duplications_index_id_seq'::regclass),
  "hash" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "index_in_file" int4 NOT NULL,
  "start_line" int4 NOT NULL,
  "end_line" int4 NOT NULL,
  "component_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "analysis_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for es_queue
-- ----------------------------
DROP TABLE IF EXISTS "public"."es_queue";
CREATE TABLE "public"."es_queue" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "doc_type" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "doc_id" varchar(4000) COLLATE "pg_catalog"."default" NOT NULL,
  "doc_id_type" varchar(20) COLLATE "pg_catalog"."default",
  "doc_routing" varchar(4000) COLLATE "pg_catalog"."default",
  "created_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for event_component_changes
-- ----------------------------
DROP TABLE IF EXISTS "public"."event_component_changes";
CREATE TABLE "public"."event_component_changes" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "event_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "event_component_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "event_analysis_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "change_category" varchar(12) COLLATE "pg_catalog"."default" NOT NULL,
  "component_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "component_key" varchar(400) COLLATE "pg_catalog"."default" NOT NULL,
  "component_name" varchar(2000) COLLATE "pg_catalog"."default" NOT NULL,
  "component_branch_key" varchar(255) COLLATE "pg_catalog"."default",
  "created_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for events
-- ----------------------------
DROP TABLE IF EXISTS "public"."events";
CREATE TABLE "public"."events" (
  "id" int4 NOT NULL DEFAULT nextval('events_id_seq'::regclass),
  "name" varchar(400) COLLATE "pg_catalog"."default",
  "category" varchar(50) COLLATE "pg_catalog"."default",
  "description" varchar(4000) COLLATE "pg_catalog"."default",
  "event_data" varchar(4000) COLLATE "pg_catalog"."default",
  "event_date" int8 NOT NULL,
  "created_at" int8 NOT NULL,
  "component_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "analysis_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for file_sources
-- ----------------------------
DROP TABLE IF EXISTS "public"."file_sources";
CREATE TABLE "public"."file_sources" (
  "id" int4 NOT NULL DEFAULT nextval('file_sources_id_seq'::regclass),
  "project_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "file_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "line_hashes" text COLLATE "pg_catalog"."default",
  "data_hash" varchar(50) COLLATE "pg_catalog"."default",
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL,
  "src_hash" varchar(50) COLLATE "pg_catalog"."default",
  "binary_data" bytea,
  "revision" varchar(100) COLLATE "pg_catalog"."default",
  "line_hashes_version" int4,
  "line_count" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for group_roles
-- ----------------------------
DROP TABLE IF EXISTS "public"."group_roles";
CREATE TABLE "public"."group_roles" (
  "id" int4 NOT NULL DEFAULT nextval('group_roles_id_seq'::regclass),
  "group_id" int4,
  "resource_id" int4,
  "role" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "organization_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for groups
-- ----------------------------
DROP TABLE IF EXISTS "public"."groups";
CREATE TABLE "public"."groups" (
  "id" int4 NOT NULL DEFAULT nextval('groups_id_seq'::regclass),
  "name" varchar(500) COLLATE "pg_catalog"."default",
  "description" varchar(200) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "organization_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for groups_users
-- ----------------------------
DROP TABLE IF EXISTS "public"."groups_users";
CREATE TABLE "public"."groups_users" (
  "user_id" int8,
  "group_id" int8
)
;

-- ----------------------------
-- Table structure for internal_properties
-- ----------------------------
DROP TABLE IF EXISTS "public"."internal_properties";
CREATE TABLE "public"."internal_properties" (
  "kee" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "is_empty" bool NOT NULL,
  "text_value" varchar(4000) COLLATE "pg_catalog"."default",
  "clob_value" text COLLATE "pg_catalog"."default",
  "created_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for issue_changes
-- ----------------------------
DROP TABLE IF EXISTS "public"."issue_changes";
CREATE TABLE "public"."issue_changes" (
  "id" int8 NOT NULL DEFAULT nextval('issue_changes_id_seq'::regclass),
  "kee" varchar(50) COLLATE "pg_catalog"."default",
  "issue_key" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "user_login" varchar(255) COLLATE "pg_catalog"."default",
  "change_type" varchar(20) COLLATE "pg_catalog"."default",
  "change_data" text COLLATE "pg_catalog"."default",
  "created_at" int8,
  "updated_at" int8,
  "issue_change_creation_date" int8
)
;

-- ----------------------------
-- Table structure for issues
-- ----------------------------
DROP TABLE IF EXISTS "public"."issues";
CREATE TABLE "public"."issues" (
  "id" int8 NOT NULL DEFAULT nextval('issues_id_seq'::regclass),
  "kee" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "rule_id" int4,
  "severity" varchar(10) COLLATE "pg_catalog"."default",
  "manual_severity" bool NOT NULL,
  "message" varchar(4000) COLLATE "pg_catalog"."default",
  "line" int4,
  "gap" numeric(30,20),
  "status" varchar(20) COLLATE "pg_catalog"."default",
  "resolution" varchar(20) COLLATE "pg_catalog"."default",
  "checksum" varchar(1000) COLLATE "pg_catalog"."default",
  "reporter" varchar(255) COLLATE "pg_catalog"."default",
  "assignee" varchar(255) COLLATE "pg_catalog"."default",
  "author_login" varchar(255) COLLATE "pg_catalog"."default",
  "action_plan_key" varchar(50) COLLATE "pg_catalog"."default",
  "issue_attributes" varchar(4000) COLLATE "pg_catalog"."default",
  "effort" int4,
  "created_at" int8,
  "updated_at" int8,
  "issue_creation_date" int8,
  "issue_update_date" int8,
  "issue_close_date" int8,
  "tags" varchar(4000) COLLATE "pg_catalog"."default",
  "component_uuid" varchar(50) COLLATE "pg_catalog"."default",
  "project_uuid" varchar(50) COLLATE "pg_catalog"."default",
  "locations" bytea,
  "issue_type" int2,
  "from_hotspot" bool
)
;

-- ----------------------------
-- Table structure for live_measures
-- ----------------------------
DROP TABLE IF EXISTS "public"."live_measures";
CREATE TABLE "public"."live_measures" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "project_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "component_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "metric_id" int4 NOT NULL,
  "value" numeric(38,20),
  "text_value" varchar(4000) COLLATE "pg_catalog"."default",
  "variation" numeric(38,20),
  "measure_data" bytea,
  "update_marker" varchar(40) COLLATE "pg_catalog"."default",
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for manual_measures
-- ----------------------------
DROP TABLE IF EXISTS "public"."manual_measures";
CREATE TABLE "public"."manual_measures" (
  "id" int8 NOT NULL DEFAULT nextval('manual_measures_id_seq'::regclass),
  "metric_id" int4 NOT NULL,
  "value" numeric(38,20),
  "text_value" varchar(4000) COLLATE "pg_catalog"."default",
  "user_uuid" varchar(255) COLLATE "pg_catalog"."default",
  "description" varchar(4000) COLLATE "pg_catalog"."default",
  "created_at" int8,
  "updated_at" int8,
  "component_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for metrics
-- ----------------------------
DROP TABLE IF EXISTS "public"."metrics";
CREATE TABLE "public"."metrics" (
  "id" int4 NOT NULL DEFAULT nextval('metrics_id_seq'::regclass),
  "name" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(255) COLLATE "pg_catalog"."default",
  "direction" int4 NOT NULL DEFAULT 0,
  "domain" varchar(64) COLLATE "pg_catalog"."default",
  "short_name" varchar(64) COLLATE "pg_catalog"."default",
  "qualitative" bool NOT NULL DEFAULT false,
  "val_type" varchar(8) COLLATE "pg_catalog"."default",
  "user_managed" bool DEFAULT false,
  "enabled" bool DEFAULT true,
  "worst_value" numeric(38,20),
  "best_value" numeric(38,20),
  "optimized_best_value" bool,
  "hidden" bool,
  "delete_historical_data" bool,
  "decimal_scale" int4
)
;

-- ----------------------------
-- Table structure for notifications
-- ----------------------------
DROP TABLE IF EXISTS "public"."notifications";
CREATE TABLE "public"."notifications" (
  "id" int4 NOT NULL DEFAULT nextval('notifications_id_seq'::regclass),
  "data" bytea
)
;

-- ----------------------------
-- Table structure for org_qprofiles
-- ----------------------------
DROP TABLE IF EXISTS "public"."org_qprofiles";
CREATE TABLE "public"."org_qprofiles" (
  "uuid" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "organization_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "rules_profile_uuid" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "parent_uuid" varchar(255) COLLATE "pg_catalog"."default",
  "last_used" int8,
  "user_updated_at" int8,
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for org_quality_gates
-- ----------------------------
DROP TABLE IF EXISTS "public"."org_quality_gates";
CREATE TABLE "public"."org_quality_gates" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "organization_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "quality_gate_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for organization_alm_bindings
-- ----------------------------
DROP TABLE IF EXISTS "public"."organization_alm_bindings";
CREATE TABLE "public"."organization_alm_bindings" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "organization_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "alm_app_install_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "alm_id" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "url" varchar(2000) COLLATE "pg_catalog"."default" NOT NULL,
  "user_uuid" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" int8 NOT NULL,
  "members_sync_enabled" bool
)
;

-- ----------------------------
-- Table structure for organization_members
-- ----------------------------
DROP TABLE IF EXISTS "public"."organization_members";
CREATE TABLE "public"."organization_members" (
  "organization_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "user_id" int4 NOT NULL
)
;

-- ----------------------------
-- Table structure for organizations
-- ----------------------------
DROP TABLE IF EXISTS "public"."organizations";
CREATE TABLE "public"."organizations" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "kee" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(256) COLLATE "pg_catalog"."default",
  "url" varchar(256) COLLATE "pg_catalog"."default",
  "avatar_url" varchar(256) COLLATE "pg_catalog"."default",
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL,
  "default_perm_template_project" varchar(40) COLLATE "pg_catalog"."default",
  "guarded" bool NOT NULL,
  "default_group_id" int4,
  "new_project_private" bool NOT NULL,
  "default_quality_gate_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "subscription" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "default_perm_template_app" varchar(40) COLLATE "pg_catalog"."default",
  "default_perm_template_port" varchar(40) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for perm_templates_groups
-- ----------------------------
DROP TABLE IF EXISTS "public"."perm_templates_groups";
CREATE TABLE "public"."perm_templates_groups" (
  "id" int4 NOT NULL DEFAULT nextval('perm_templates_groups_id_seq'::regclass),
  "group_id" int4,
  "template_id" int4 NOT NULL,
  "permission_reference" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(6),
  "updated_at" timestamp(6)
)
;

-- ----------------------------
-- Table structure for perm_templates_users
-- ----------------------------
DROP TABLE IF EXISTS "public"."perm_templates_users";
CREATE TABLE "public"."perm_templates_users" (
  "id" int4 NOT NULL DEFAULT nextval('perm_templates_users_id_seq'::regclass),
  "user_id" int4 NOT NULL,
  "template_id" int4 NOT NULL,
  "permission_reference" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(6),
  "updated_at" timestamp(6)
)
;

-- ----------------------------
-- Table structure for perm_tpl_characteristics
-- ----------------------------
DROP TABLE IF EXISTS "public"."perm_tpl_characteristics";
CREATE TABLE "public"."perm_tpl_characteristics" (
  "id" int4 NOT NULL DEFAULT nextval('perm_tpl_characteristics_id_seq'::regclass),
  "template_id" int4 NOT NULL,
  "permission_key" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "with_project_creator" bool NOT NULL DEFAULT false,
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for permission_templates
-- ----------------------------
DROP TABLE IF EXISTS "public"."permission_templates";
CREATE TABLE "public"."permission_templates" (
  "id" int4 NOT NULL DEFAULT nextval('permission_templates_id_seq'::regclass),
  "name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "kee" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(4000) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "key_pattern" varchar(500) COLLATE "pg_catalog"."default",
  "organization_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for plugins
-- ----------------------------
DROP TABLE IF EXISTS "public"."plugins";
CREATE TABLE "public"."plugins" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "kee" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "base_plugin_key" varchar(200) COLLATE "pg_catalog"."default",
  "file_hash" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for project_alm_bindings
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_alm_bindings";
CREATE TABLE "public"."project_alm_bindings" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "alm_id" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "repo_id" varchar(256) COLLATE "pg_catalog"."default" NOT NULL,
  "project_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "github_slug" varchar(256) COLLATE "pg_catalog"."default",
  "url" varchar(2000) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for project_branches
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_branches";
CREATE TABLE "public"."project_branches" (
  "uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "project_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "kee" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "branch_type" varchar(12) COLLATE "pg_catalog"."default",
  "merge_branch_uuid" varchar(50) COLLATE "pg_catalog"."default",
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL,
  "key_type" varchar(12) COLLATE "pg_catalog"."default" NOT NULL,
  "pull_request_binary" bytea,
  "manual_baseline_analysis_uuid" varchar(40) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for project_links
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_links";
CREATE TABLE "public"."project_links" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "project_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "link_type" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(128) COLLATE "pg_catalog"."default",
  "href" varchar(2048) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for project_mappings
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_mappings";
CREATE TABLE "public"."project_mappings" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "key_type" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "kee" varchar(4000) COLLATE "pg_catalog"."default" NOT NULL,
  "project_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for project_measures
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_measures";
CREATE TABLE "public"."project_measures" (
  "id" int8 NOT NULL DEFAULT nextval('project_measures_id_seq'::regclass),
  "value" numeric(38,20),
  "metric_id" int4 NOT NULL,
  "text_value" varchar(4000) COLLATE "pg_catalog"."default",
  "alert_status" varchar(5) COLLATE "pg_catalog"."default",
  "alert_text" varchar(4000) COLLATE "pg_catalog"."default",
  "description" varchar(4000) COLLATE "pg_catalog"."default",
  "person_id" int4,
  "variation_value_1" numeric(38,20),
  "variation_value_2" numeric(38,20),
  "variation_value_3" numeric(38,20),
  "variation_value_4" numeric(38,20),
  "variation_value_5" numeric(38,20),
  "measure_data" bytea,
  "component_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "analysis_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for project_qprofiles
-- ----------------------------
DROP TABLE IF EXISTS "public"."project_qprofiles";
CREATE TABLE "public"."project_qprofiles" (
  "id" int4 NOT NULL DEFAULT nextval('project_qprofiles_id_seq'::regclass),
  "project_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "profile_key" varchar(50) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for projects
-- ----------------------------
DROP TABLE IF EXISTS "public"."projects";
CREATE TABLE "public"."projects" (
  "id" int4 NOT NULL DEFAULT nextval('projects_id_seq'::regclass),
  "name" varchar(2000) COLLATE "pg_catalog"."default",
  "description" varchar(2000) COLLATE "pg_catalog"."default",
  "enabled" bool NOT NULL DEFAULT true,
  "scope" varchar(3) COLLATE "pg_catalog"."default",
  "qualifier" varchar(10) COLLATE "pg_catalog"."default",
  "kee" varchar(400) COLLATE "pg_catalog"."default",
  "language" varchar(20) COLLATE "pg_catalog"."default",
  "long_name" varchar(2000) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "path" varchar(2000) COLLATE "pg_catalog"."default",
  "deprecated_kee" varchar(400) COLLATE "pg_catalog"."default",
  "uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "project_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "module_uuid" varchar(50) COLLATE "pg_catalog"."default",
  "module_uuid_path" varchar(1500) COLLATE "pg_catalog"."default",
  "authorization_updated_at" int8,
  "root_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "copy_component_uuid" varchar(50) COLLATE "pg_catalog"."default",
  "developer_uuid" varchar(50) COLLATE "pg_catalog"."default",
  "uuid_path" varchar(1500) COLLATE "pg_catalog"."default" NOT NULL,
  "b_changed" bool,
  "b_copy_component_uuid" varchar(50) COLLATE "pg_catalog"."default",
  "b_description" varchar(2000) COLLATE "pg_catalog"."default",
  "b_enabled" bool,
  "b_language" varchar(20) COLLATE "pg_catalog"."default",
  "b_long_name" varchar(500) COLLATE "pg_catalog"."default",
  "b_module_uuid" varchar(50) COLLATE "pg_catalog"."default",
  "b_module_uuid_path" varchar(1500) COLLATE "pg_catalog"."default",
  "b_name" varchar(500) COLLATE "pg_catalog"."default",
  "b_path" varchar(2000) COLLATE "pg_catalog"."default",
  "b_qualifier" varchar(10) COLLATE "pg_catalog"."default",
  "b_uuid_path" varchar(1500) COLLATE "pg_catalog"."default",
  "organization_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "tags" varchar(500) COLLATE "pg_catalog"."default",
  "private" bool NOT NULL,
  "main_branch_project_uuid" varchar(50) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for properties
-- ----------------------------
DROP TABLE IF EXISTS "public"."properties";
CREATE TABLE "public"."properties" (
  "id" int4 NOT NULL DEFAULT nextval('properties2_id_seq'::regclass),
  "prop_key" varchar(512) COLLATE "pg_catalog"."default" NOT NULL,
  "resource_id" int8,
  "user_id" int8,
  "is_empty" bool NOT NULL,
  "text_value" varchar(4000) COLLATE "pg_catalog"."default",
  "clob_value" text COLLATE "pg_catalog"."default",
  "created_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for qprofile_changes
-- ----------------------------
DROP TABLE IF EXISTS "public"."qprofile_changes";
CREATE TABLE "public"."qprofile_changes" (
  "kee" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "rules_profile_uuid" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "change_type" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "user_uuid" varchar(255) COLLATE "pg_catalog"."default",
  "change_data" text COLLATE "pg_catalog"."default",
  "created_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for qprofile_edit_groups
-- ----------------------------
DROP TABLE IF EXISTS "public"."qprofile_edit_groups";
CREATE TABLE "public"."qprofile_edit_groups" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "group_id" int4 NOT NULL,
  "qprofile_uuid" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for qprofile_edit_users
-- ----------------------------
DROP TABLE IF EXISTS "public"."qprofile_edit_users";
CREATE TABLE "public"."qprofile_edit_users" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "user_id" int4 NOT NULL,
  "qprofile_uuid" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for quality_gate_conditions
-- ----------------------------
DROP TABLE IF EXISTS "public"."quality_gate_conditions";
CREATE TABLE "public"."quality_gate_conditions" (
  "id" int4 NOT NULL DEFAULT nextval('quality_gate_conditions_id_seq'::regclass),
  "qgate_id" int4,
  "metric_id" int4,
  "period" int4,
  "operator" varchar(3) COLLATE "pg_catalog"."default",
  "value_error" varchar(64) COLLATE "pg_catalog"."default",
  "value_warning" varchar(64) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6)
)
;

-- ----------------------------
-- Table structure for quality_gates
-- ----------------------------
DROP TABLE IF EXISTS "public"."quality_gates";
CREATE TABLE "public"."quality_gates" (
  "id" int4 NOT NULL DEFAULT nextval('quality_gates_id_seq'::regclass),
  "name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "is_built_in" bool NOT NULL,
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for rule_repositories
-- ----------------------------
DROP TABLE IF EXISTS "public"."rule_repositories";
CREATE TABLE "public"."rule_repositories" (
  "kee" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "language" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(4000) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for rules
-- ----------------------------
DROP TABLE IF EXISTS "public"."rules";
CREATE TABLE "public"."rules" (
  "id" int4 NOT NULL DEFAULT nextval('rules_id_seq'::regclass),
  "name" varchar(200) COLLATE "pg_catalog"."default",
  "plugin_rule_key" varchar(200) COLLATE "pg_catalog"."default" NOT NULL,
  "plugin_config_key" varchar(200) COLLATE "pg_catalog"."default",
  "plugin_name" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "description" text COLLATE "pg_catalog"."default",
  "priority" int4,
  "template_id" int4,
  "status" varchar(40) COLLATE "pg_catalog"."default",
  "language" varchar(20) COLLATE "pg_catalog"."default",
  "def_remediation_function" varchar(20) COLLATE "pg_catalog"."default",
  "def_remediation_gap_mult" varchar(20) COLLATE "pg_catalog"."default",
  "def_remediation_base_effort" varchar(20) COLLATE "pg_catalog"."default",
  "gap_description" varchar(4000) COLLATE "pg_catalog"."default",
  "system_tags" varchar(4000) COLLATE "pg_catalog"."default",
  "is_template" bool NOT NULL DEFAULT false,
  "description_format" varchar(20) COLLATE "pg_catalog"."default",
  "created_at" int8,
  "updated_at" int8,
  "rule_type" int2,
  "plugin_key" varchar(200) COLLATE "pg_catalog"."default",
  "scope" varchar(20) COLLATE "pg_catalog"."default" NOT NULL,
  "is_external" bool NOT NULL,
  "security_standards" varchar(4000) COLLATE "pg_catalog"."default",
  "is_ad_hoc" bool NOT NULL
)
;

-- ----------------------------
-- Table structure for rules_metadata
-- ----------------------------
DROP TABLE IF EXISTS "public"."rules_metadata";
CREATE TABLE "public"."rules_metadata" (
  "rule_id" int4 NOT NULL,
  "organization_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "note_data" text COLLATE "pg_catalog"."default",
  "note_user_uuid" varchar(255) COLLATE "pg_catalog"."default",
  "note_created_at" int8,
  "note_updated_at" int8,
  "remediation_function" varchar(20) COLLATE "pg_catalog"."default",
  "remediation_gap_mult" varchar(20) COLLATE "pg_catalog"."default",
  "remediation_base_effort" varchar(20) COLLATE "pg_catalog"."default",
  "tags" varchar(4000) COLLATE "pg_catalog"."default",
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL,
  "ad_hoc_name" varchar(200) COLLATE "pg_catalog"."default",
  "ad_hoc_description" text COLLATE "pg_catalog"."default",
  "ad_hoc_severity" varchar(10) COLLATE "pg_catalog"."default",
  "ad_hoc_type" int2
)
;

-- ----------------------------
-- Table structure for rules_parameters
-- ----------------------------
DROP TABLE IF EXISTS "public"."rules_parameters";
CREATE TABLE "public"."rules_parameters" (
  "id" int4 NOT NULL DEFAULT nextval('rules_parameters_id_seq'::regclass),
  "rule_id" int4 NOT NULL,
  "name" varchar(128) COLLATE "pg_catalog"."default" NOT NULL,
  "description" varchar(4000) COLLATE "pg_catalog"."default",
  "param_type" varchar(512) COLLATE "pg_catalog"."default" NOT NULL,
  "default_value" varchar(4000) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for rules_profiles
-- ----------------------------
DROP TABLE IF EXISTS "public"."rules_profiles";
CREATE TABLE "public"."rules_profiles" (
  "id" int4 NOT NULL DEFAULT nextval('rules_profiles_id_seq'::regclass),
  "name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "language" varchar(20) COLLATE "pg_catalog"."default",
  "kee" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "rules_updated_at" varchar(100) COLLATE "pg_catalog"."default",
  "created_at" timestamp(6),
  "updated_at" timestamp(6),
  "is_built_in" bool NOT NULL
)
;

-- ----------------------------
-- Table structure for schema_migrations
-- ----------------------------
DROP TABLE IF EXISTS "public"."schema_migrations";
CREATE TABLE "public"."schema_migrations" (
  "version" varchar(255) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for snapshots
-- ----------------------------
DROP TABLE IF EXISTS "public"."snapshots";
CREATE TABLE "public"."snapshots" (
  "id" int4 NOT NULL DEFAULT nextval('snapshots_id_seq'::regclass),
  "status" varchar(4) COLLATE "pg_catalog"."default" NOT NULL DEFAULT 'U'::character varying,
  "islast" bool NOT NULL DEFAULT false,
  "version" varchar(500) COLLATE "pg_catalog"."default",
  "purge_status" int4,
  "period1_mode" varchar(100) COLLATE "pg_catalog"."default",
  "period1_param" varchar(100) COLLATE "pg_catalog"."default",
  "period2_mode" varchar(100) COLLATE "pg_catalog"."default",
  "period2_param" varchar(100) COLLATE "pg_catalog"."default",
  "period3_mode" varchar(100) COLLATE "pg_catalog"."default",
  "period3_param" varchar(100) COLLATE "pg_catalog"."default",
  "period4_mode" varchar(100) COLLATE "pg_catalog"."default",
  "period4_param" varchar(100) COLLATE "pg_catalog"."default",
  "period5_mode" varchar(100) COLLATE "pg_catalog"."default",
  "period5_param" varchar(100) COLLATE "pg_catalog"."default",
  "created_at" int8,
  "build_date" int8,
  "period1_date" int8,
  "period2_date" int8,
  "period3_date" int8,
  "period4_date" int8,
  "period5_date" int8,
  "component_uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "uuid" varchar(50) COLLATE "pg_catalog"."default" NOT NULL,
  "build_string" varchar(100) COLLATE "pg_catalog"."default"
)
;

-- ----------------------------
-- Table structure for user_properties
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_properties";
CREATE TABLE "public"."user_properties" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "user_uuid" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "kee" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "text_value" varchar(4000) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" int8 NOT NULL,
  "updated_at" int8 NOT NULL
)
;

-- ----------------------------
-- Table structure for user_roles
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_roles";
CREATE TABLE "public"."user_roles" (
  "id" int4 NOT NULL DEFAULT nextval('user_roles_id_seq'::regclass),
  "user_id" int4,
  "resource_id" int4,
  "role" varchar(64) COLLATE "pg_catalog"."default" NOT NULL,
  "organization_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for user_tokens
-- ----------------------------
DROP TABLE IF EXISTS "public"."user_tokens";
CREATE TABLE "public"."user_tokens" (
  "id" int4 NOT NULL DEFAULT nextval('user_tokens_id_seq'::regclass),
  "user_uuid" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "token_hash" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "created_at" int8 NOT NULL,
  "last_connection_date" int8
)
;

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS "public"."users";
CREATE TABLE "public"."users" (
  "id" int4 NOT NULL DEFAULT nextval('users_id_seq'::regclass),
  "login" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(200) COLLATE "pg_catalog"."default",
  "email" varchar(100) COLLATE "pg_catalog"."default",
  "crypted_password" varchar(100) COLLATE "pg_catalog"."default",
  "salt" varchar(40) COLLATE "pg_catalog"."default",
  "active" bool DEFAULT true,
  "created_at" int8,
  "updated_at" int8,
  "scm_accounts" varchar(4000) COLLATE "pg_catalog"."default",
  "external_login" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "external_identity_provider" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "user_local" bool,
  "is_root" bool NOT NULL,
  "onboarded" bool NOT NULL,
  "homepage_type" varchar(40) COLLATE "pg_catalog"."default",
  "homepage_parameter" varchar(40) COLLATE "pg_catalog"."default",
  "hash_method" varchar(10) COLLATE "pg_catalog"."default",
  "uuid" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "external_id" varchar(255) COLLATE "pg_catalog"."default" NOT NULL,
  "organization_uuid" varchar(40) COLLATE "pg_catalog"."default",
  "last_connection_date" int8
)
;

-- ----------------------------
-- Table structure for webhook_deliveries
-- ----------------------------
DROP TABLE IF EXISTS "public"."webhook_deliveries";
CREATE TABLE "public"."webhook_deliveries" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "component_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "ce_task_uuid" varchar(40) COLLATE "pg_catalog"."default",
  "name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "url" varchar(2000) COLLATE "pg_catalog"."default" NOT NULL,
  "success" bool NOT NULL,
  "http_status" int4,
  "duration_ms" int8 NOT NULL,
  "payload" text COLLATE "pg_catalog"."default" NOT NULL,
  "error_stacktrace" text COLLATE "pg_catalog"."default",
  "created_at" int8 NOT NULL,
  "analysis_uuid" varchar(40) COLLATE "pg_catalog"."default",
  "webhook_uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL
)
;

-- ----------------------------
-- Table structure for webhooks
-- ----------------------------
DROP TABLE IF EXISTS "public"."webhooks";
CREATE TABLE "public"."webhooks" (
  "uuid" varchar(40) COLLATE "pg_catalog"."default" NOT NULL,
  "name" varchar(100) COLLATE "pg_catalog"."default" NOT NULL,
  "url" varchar(2000) COLLATE "pg_catalog"."default" NOT NULL,
  "organization_uuid" varchar(40) COLLATE "pg_catalog"."default",
  "project_uuid" varchar(40) COLLATE "pg_catalog"."default",
  "created_at" int8 NOT NULL,
  "updated_at" int8
)
;

-- ----------------------------
-- Indexes structure for table active_rule_parameters
-- ----------------------------
CREATE INDEX "ix_arp_on_active_rule_id" ON "public"."active_rule_parameters" USING btree (
  "active_rule_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table active_rule_parameters
-- ----------------------------
ALTER TABLE "public"."active_rule_parameters" ADD CONSTRAINT "pk_active_rule_parameters" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table active_rules
-- ----------------------------
CREATE UNIQUE INDEX "uniq_profile_rule_ids" ON "public"."active_rules" USING btree (
  "profile_id" "pg_catalog"."int4_ops" ASC NULLS LAST,
  "rule_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table active_rules
-- ----------------------------
ALTER TABLE "public"."active_rules" ADD CONSTRAINT "pk_active_rules" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table alm_app_installs
-- ----------------------------
CREATE INDEX "alm_app_installs_external_id" ON "public"."alm_app_installs" USING btree (
  "user_external_id" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "alm_app_installs_install" ON "public"."alm_app_installs" USING btree (
  "alm_id" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "install_id" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "alm_app_installs_owner" ON "public"."alm_app_installs" USING btree (
  "alm_id" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "owner_id" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table alm_app_installs
-- ----------------------------
ALTER TABLE "public"."alm_app_installs" ADD CONSTRAINT "pk_alm_app_installs" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table analysis_properties
-- ----------------------------
CREATE INDEX "ix_snapshot_uuid" ON "public"."analysis_properties" USING btree (
  "snapshot_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table analysis_properties
-- ----------------------------
ALTER TABLE "public"."analysis_properties" ADD CONSTRAINT "pk_analysis_properties" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table ce_activity
-- ----------------------------
CREATE INDEX "ce_activity_component" ON "public"."ce_activity" USING btree (
  "component_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "ce_activity_islast" ON "public"."ce_activity" USING btree (
  "is_last" "pg_catalog"."bool_ops" ASC NULLS LAST,
  "status" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "ce_activity_islast_key" ON "public"."ce_activity" USING btree (
  "is_last_key" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "ce_activity_main_component" ON "public"."ce_activity" USING btree (
  "main_component_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "ce_activity_main_islast" ON "public"."ce_activity" USING btree (
  "main_is_last" "pg_catalog"."bool_ops" ASC NULLS LAST,
  "status" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "ce_activity_main_islast_key" ON "public"."ce_activity" USING btree (
  "main_is_last_key" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "ce_activity_uuid" ON "public"."ce_activity" USING btree (
  "uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table ce_activity
-- ----------------------------
ALTER TABLE "public"."ce_activity" ADD CONSTRAINT "pk_ce_activity" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table ce_queue
-- ----------------------------
CREATE INDEX "ce_queue_component" ON "public"."ce_queue" USING btree (
  "component_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "ce_queue_main_component" ON "public"."ce_queue" USING btree (
  "main_component_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "ce_queue_uuid" ON "public"."ce_queue" USING btree (
  "uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table ce_queue
-- ----------------------------
ALTER TABLE "public"."ce_queue" ADD CONSTRAINT "pk_ce_queue" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table ce_scanner_context
-- ----------------------------
ALTER TABLE "public"."ce_scanner_context" ADD CONSTRAINT "pk_ce_scanner_context" PRIMARY KEY ("task_uuid");

-- ----------------------------
-- Indexes structure for table ce_task_characteristics
-- ----------------------------
CREATE INDEX "ce_characteristics_task_uuid" ON "public"."ce_task_characteristics" USING btree (
  "task_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table ce_task_characteristics
-- ----------------------------
ALTER TABLE "public"."ce_task_characteristics" ADD CONSTRAINT "pk_ce_task_characteristics" PRIMARY KEY ("uuid");

-- ----------------------------
-- Primary Key structure for table ce_task_input
-- ----------------------------
ALTER TABLE "public"."ce_task_input" ADD CONSTRAINT "pk_ce_task_input" PRIMARY KEY ("task_uuid");

-- ----------------------------
-- Indexes structure for table ce_task_message
-- ----------------------------
CREATE INDEX "ce_task_message_task" ON "public"."ce_task_message" USING btree (
  "task_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table ce_task_message
-- ----------------------------
ALTER TABLE "public"."ce_task_message" ADD CONSTRAINT "pk_ce_task_message" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table default_qprofiles
-- ----------------------------
CREATE UNIQUE INDEX "uniq_default_qprofiles_uuid" ON "public"."default_qprofiles" USING btree (
  "qprofile_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table default_qprofiles
-- ----------------------------
ALTER TABLE "public"."default_qprofiles" ADD CONSTRAINT "pk_default_qprofiles" PRIMARY KEY ("organization_uuid", "language");

-- ----------------------------
-- Indexes structure for table deprecated_rule_keys
-- ----------------------------
CREATE UNIQUE INDEX "rule_id_deprecated_rule_keys" ON "public"."deprecated_rule_keys" USING btree (
  "rule_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "uniq_deprecated_rule_keys" ON "public"."deprecated_rule_keys" USING btree (
  "old_repository_key" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "old_rule_key" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table deprecated_rule_keys
-- ----------------------------
ALTER TABLE "public"."deprecated_rule_keys" ADD CONSTRAINT "pk_deprecated_rule_keys" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table duplications_index
-- ----------------------------
CREATE INDEX "duplication_analysis_component" ON "public"."duplications_index" USING btree (
  "analysis_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "component_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "duplications_index_hash" ON "public"."duplications_index" USING btree (
  "hash" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table duplications_index
-- ----------------------------
ALTER TABLE "public"."duplications_index" ADD CONSTRAINT "pk_duplications_index" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table es_queue
-- ----------------------------
CREATE INDEX "es_queue_created_at" ON "public"."es_queue" USING btree (
  "created_at" "pg_catalog"."int8_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table es_queue
-- ----------------------------
ALTER TABLE "public"."es_queue" ADD CONSTRAINT "pk_es_queue" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table event_component_changes
-- ----------------------------
CREATE UNIQUE INDEX "event_component_changes_unique" ON "public"."event_component_changes" USING btree (
  "event_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "change_category" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "component_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "event_cpnt_changes_analysis" ON "public"."event_component_changes" USING btree (
  "event_analysis_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "event_cpnt_changes_cpnt" ON "public"."event_component_changes" USING btree (
  "event_component_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table event_component_changes
-- ----------------------------
ALTER TABLE "public"."event_component_changes" ADD CONSTRAINT "pk_event_component_changes" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table events
-- ----------------------------
CREATE INDEX "events_analysis" ON "public"."events" USING btree (
  "analysis_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "events_component_uuid" ON "public"."events" USING btree (
  "component_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "events_uuid" ON "public"."events" USING btree (
  "uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table events
-- ----------------------------
ALTER TABLE "public"."events" ADD CONSTRAINT "pk_events" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table file_sources
-- ----------------------------
CREATE UNIQUE INDEX "file_sources_file_uuid" ON "public"."file_sources" USING btree (
  "file_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "file_sources_project_uuid" ON "public"."file_sources" USING btree (
  "project_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "file_sources_updated_at" ON "public"."file_sources" USING btree (
  "updated_at" "pg_catalog"."int8_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table file_sources
-- ----------------------------
ALTER TABLE "public"."file_sources" ADD CONSTRAINT "pk_file_sources" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table group_roles
-- ----------------------------
CREATE INDEX "group_roles_resource" ON "public"."group_roles" USING btree (
  "resource_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "uniq_group_roles" ON "public"."group_roles" USING btree (
  "organization_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "group_id" "pg_catalog"."int4_ops" ASC NULLS LAST,
  "resource_id" "pg_catalog"."int4_ops" ASC NULLS LAST,
  "role" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table group_roles
-- ----------------------------
ALTER TABLE "public"."group_roles" ADD CONSTRAINT "pk_group_roles" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table groups
-- ----------------------------
ALTER TABLE "public"."groups" ADD CONSTRAINT "pk_groups" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table groups_users
-- ----------------------------
CREATE UNIQUE INDEX "groups_users_unique" ON "public"."groups_users" USING btree (
  "group_id" "pg_catalog"."int8_ops" ASC NULLS LAST,
  "user_id" "pg_catalog"."int8_ops" ASC NULLS LAST
);
CREATE INDEX "index_groups_users_on_group_id" ON "public"."groups_users" USING btree (
  "group_id" "pg_catalog"."int8_ops" ASC NULLS LAST
);
CREATE INDEX "index_groups_users_on_user_id" ON "public"."groups_users" USING btree (
  "user_id" "pg_catalog"."int8_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table internal_properties
-- ----------------------------
ALTER TABLE "public"."internal_properties" ADD CONSTRAINT "pk_internal_properties" PRIMARY KEY ("kee");

-- ----------------------------
-- Indexes structure for table issue_changes
-- ----------------------------
CREATE INDEX "issue_changes_issue_key" ON "public"."issue_changes" USING btree (
  "issue_key" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "issue_changes_kee" ON "public"."issue_changes" USING btree (
  "kee" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table issue_changes
-- ----------------------------
ALTER TABLE "public"."issue_changes" ADD CONSTRAINT "pk_issue_changes" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table issues
-- ----------------------------
CREATE INDEX "issues_assignee" ON "public"."issues" USING btree (
  "assignee" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "issues_component_uuid" ON "public"."issues" USING btree (
  "component_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "issues_creation_date" ON "public"."issues" USING btree (
  "issue_creation_date" "pg_catalog"."int8_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "issues_kee" ON "public"."issues" USING btree (
  "kee" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "issues_project_uuid" ON "public"."issues" USING btree (
  "project_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "issues_resolution" ON "public"."issues" USING btree (
  "resolution" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "issues_rule_id" ON "public"."issues" USING btree (
  "rule_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "issues_updated_at" ON "public"."issues" USING btree (
  "updated_at" "pg_catalog"."int8_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table issues
-- ----------------------------
ALTER TABLE "public"."issues" ADD CONSTRAINT "pk_issues" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table live_measures
-- ----------------------------
CREATE UNIQUE INDEX "live_measures_component" ON "public"."live_measures" USING btree (
  "component_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "metric_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "live_measures_project" ON "public"."live_measures" USING btree (
  "project_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table live_measures
-- ----------------------------
ALTER TABLE "public"."live_measures" ADD CONSTRAINT "pk_live_measures" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table manual_measures
-- ----------------------------
CREATE INDEX "manual_measures_component_uuid" ON "public"."manual_measures" USING btree (
  "component_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table manual_measures
-- ----------------------------
ALTER TABLE "public"."manual_measures" ADD CONSTRAINT "pk_manual_measures" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table metrics
-- ----------------------------
CREATE UNIQUE INDEX "metrics_unique_name" ON "public"."metrics" USING btree (
  "name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table metrics
-- ----------------------------
ALTER TABLE "public"."metrics" ADD CONSTRAINT "pk_metrics" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table notifications
-- ----------------------------
ALTER TABLE "public"."notifications" ADD CONSTRAINT "pk_notifications" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table org_qprofiles
-- ----------------------------
CREATE INDEX "qprofiles_org_uuid" ON "public"."org_qprofiles" USING btree (
  "organization_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "qprofiles_rp_uuid" ON "public"."org_qprofiles" USING btree (
  "rules_profile_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table org_qprofiles
-- ----------------------------
ALTER TABLE "public"."org_qprofiles" ADD CONSTRAINT "pk_org_qprofiles" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table org_quality_gates
-- ----------------------------
CREATE UNIQUE INDEX "uniq_org_quality_gates" ON "public"."org_quality_gates" USING btree (
  "organization_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "quality_gate_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table org_quality_gates
-- ----------------------------
ALTER TABLE "public"."org_quality_gates" ADD CONSTRAINT "pk_org_quality_gates" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table organization_alm_bindings
-- ----------------------------
CREATE UNIQUE INDEX "org_alm_bindings_install" ON "public"."organization_alm_bindings" USING btree (
  "alm_app_install_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "org_alm_bindings_org" ON "public"."organization_alm_bindings" USING btree (
  "organization_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table organization_alm_bindings
-- ----------------------------
ALTER TABLE "public"."organization_alm_bindings" ADD CONSTRAINT "pk_organization_alm_bindings" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table organization_members
-- ----------------------------
CREATE INDEX "ix_org_members_on_user_id" ON "public"."organization_members" USING btree (
  "user_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table organization_members
-- ----------------------------
ALTER TABLE "public"."organization_members" ADD CONSTRAINT "pk_organization_members" PRIMARY KEY ("organization_uuid", "user_id");

-- ----------------------------
-- Indexes structure for table organizations
-- ----------------------------
CREATE UNIQUE INDEX "organization_key" ON "public"."organizations" USING btree (
  "kee" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table organizations
-- ----------------------------
ALTER TABLE "public"."organizations" ADD CONSTRAINT "pk_organizations" PRIMARY KEY ("uuid");

-- ----------------------------
-- Primary Key structure for table perm_templates_groups
-- ----------------------------
ALTER TABLE "public"."perm_templates_groups" ADD CONSTRAINT "pk_perm_templates_groups" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table perm_templates_users
-- ----------------------------
ALTER TABLE "public"."perm_templates_users" ADD CONSTRAINT "pk_perm_templates_users" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table perm_tpl_characteristics
-- ----------------------------
CREATE UNIQUE INDEX "uniq_perm_tpl_charac" ON "public"."perm_tpl_characteristics" USING btree (
  "template_id" "pg_catalog"."int4_ops" ASC NULLS LAST,
  "permission_key" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table perm_tpl_characteristics
-- ----------------------------
ALTER TABLE "public"."perm_tpl_characteristics" ADD CONSTRAINT "pk_perm_tpl_characteristics" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table permission_templates
-- ----------------------------
ALTER TABLE "public"."permission_templates" ADD CONSTRAINT "pk_permission_templates" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table plugins
-- ----------------------------
CREATE UNIQUE INDEX "plugins_key" ON "public"."plugins" USING btree (
  "kee" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table plugins
-- ----------------------------
ALTER TABLE "public"."plugins" ADD CONSTRAINT "pk_plugins" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table project_alm_bindings
-- ----------------------------
CREATE UNIQUE INDEX "project_alm_bindings_alm_repo" ON "public"."project_alm_bindings" USING btree (
  "alm_id" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "repo_id" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "project_alm_bindings_project" ON "public"."project_alm_bindings" USING btree (
  "project_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table project_alm_bindings
-- ----------------------------
ALTER TABLE "public"."project_alm_bindings" ADD CONSTRAINT "pk_project_alm_bindings" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table project_branches
-- ----------------------------
CREATE UNIQUE INDEX "project_branches_kee_key_type" ON "public"."project_branches" USING btree (
  "project_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "kee" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "key_type" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table project_branches
-- ----------------------------
ALTER TABLE "public"."project_branches" ADD CONSTRAINT "pk_project_branches" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table project_links
-- ----------------------------
CREATE INDEX "project_links_project" ON "public"."project_links" USING btree (
  "project_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table project_links
-- ----------------------------
ALTER TABLE "public"."project_links" ADD CONSTRAINT "pk_project_links2" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table project_mappings
-- ----------------------------
CREATE UNIQUE INDEX "key_type_kee" ON "public"."project_mappings" USING btree (
  "key_type" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "kee" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "project_uuid" ON "public"."project_mappings" USING btree (
  "project_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table project_mappings
-- ----------------------------
ALTER TABLE "public"."project_mappings" ADD CONSTRAINT "pk_project_mappings" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table project_measures
-- ----------------------------
CREATE INDEX "measures_analysis_metric" ON "public"."project_measures" USING btree (
  "analysis_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "metric_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "measures_component_uuid" ON "public"."project_measures" USING btree (
  "component_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table project_measures
-- ----------------------------
ALTER TABLE "public"."project_measures" ADD CONSTRAINT "pk_project_measures" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table project_qprofiles
-- ----------------------------
CREATE UNIQUE INDEX "uniq_project_qprofiles" ON "public"."project_qprofiles" USING btree (
  "project_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "profile_key" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table project_qprofiles
-- ----------------------------
ALTER TABLE "public"."project_qprofiles" ADD CONSTRAINT "pk_project_qprofiles" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table projects
-- ----------------------------
CREATE UNIQUE INDEX "projects_kee" ON "public"."projects" USING btree (
  "kee" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "projects_module_uuid" ON "public"."projects" USING btree (
  "module_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "projects_organization" ON "public"."projects" USING btree (
  "organization_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "projects_project_uuid" ON "public"."projects" USING btree (
  "project_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "projects_qualifier" ON "public"."projects" USING btree (
  "qualifier" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "projects_root_uuid" ON "public"."projects" USING btree (
  "root_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "projects_uuid" ON "public"."projects" USING btree (
  "uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table projects
-- ----------------------------
ALTER TABLE "public"."projects" ADD CONSTRAINT "pk_projects" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table properties
-- ----------------------------
CREATE INDEX "properties_key" ON "public"."properties" USING btree (
  "prop_key" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table properties
-- ----------------------------
ALTER TABLE "public"."properties" ADD CONSTRAINT "pk_properties" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table qprofile_changes
-- ----------------------------
CREATE INDEX "qp_changes_rules_profile_uuid" ON "public"."qprofile_changes" USING btree (
  "rules_profile_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table qprofile_changes
-- ----------------------------
ALTER TABLE "public"."qprofile_changes" ADD CONSTRAINT "pk_qprofile_changes" PRIMARY KEY ("kee");

-- ----------------------------
-- Indexes structure for table qprofile_edit_groups
-- ----------------------------
CREATE INDEX "qprofile_edit_groups_qprofile" ON "public"."qprofile_edit_groups" USING btree (
  "qprofile_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "qprofile_edit_groups_unique" ON "public"."qprofile_edit_groups" USING btree (
  "group_id" "pg_catalog"."int4_ops" ASC NULLS LAST,
  "qprofile_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table qprofile_edit_groups
-- ----------------------------
ALTER TABLE "public"."qprofile_edit_groups" ADD CONSTRAINT "pk_qprofile_edit_groups" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table qprofile_edit_users
-- ----------------------------
CREATE INDEX "qprofile_edit_users_qprofile" ON "public"."qprofile_edit_users" USING btree (
  "qprofile_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "qprofile_edit_users_unique" ON "public"."qprofile_edit_users" USING btree (
  "user_id" "pg_catalog"."int4_ops" ASC NULLS LAST,
  "qprofile_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table qprofile_edit_users
-- ----------------------------
ALTER TABLE "public"."qprofile_edit_users" ADD CONSTRAINT "pk_qprofile_edit_users" PRIMARY KEY ("uuid");

-- ----------------------------
-- Primary Key structure for table quality_gate_conditions
-- ----------------------------
ALTER TABLE "public"."quality_gate_conditions" ADD CONSTRAINT "pk_quality_gate_conditions" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table quality_gates
-- ----------------------------
CREATE UNIQUE INDEX "uniq_quality_gates_uuid" ON "public"."quality_gates" USING btree (
  "uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table quality_gates
-- ----------------------------
ALTER TABLE "public"."quality_gates" ADD CONSTRAINT "pk_quality_gates" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table rule_repositories
-- ----------------------------
ALTER TABLE "public"."rule_repositories" ADD CONSTRAINT "pk_rule_repositories" PRIMARY KEY ("kee");

-- ----------------------------
-- Indexes structure for table rules
-- ----------------------------
CREATE UNIQUE INDEX "rules_repo_key" ON "public"."rules" USING btree (
  "plugin_rule_key" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "plugin_name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table rules
-- ----------------------------
ALTER TABLE "public"."rules" ADD CONSTRAINT "pk_rules" PRIMARY KEY ("id");

-- ----------------------------
-- Primary Key structure for table rules_metadata
-- ----------------------------
ALTER TABLE "public"."rules_metadata" ADD CONSTRAINT "pk_rules_metadata" PRIMARY KEY ("rule_id", "organization_uuid");

-- ----------------------------
-- Indexes structure for table rules_parameters
-- ----------------------------
CREATE INDEX "rules_parameters_rule_id" ON "public"."rules_parameters" USING btree (
  "rule_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "rules_parameters_unique" ON "public"."rules_parameters" USING btree (
  "rule_id" "pg_catalog"."int4_ops" ASC NULLS LAST,
  "name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table rules_parameters
-- ----------------------------
ALTER TABLE "public"."rules_parameters" ADD CONSTRAINT "pk_rules_parameters" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table rules_profiles
-- ----------------------------
CREATE UNIQUE INDEX "uniq_qprof_key" ON "public"."rules_profiles" USING btree (
  "kee" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table rules_profiles
-- ----------------------------
ALTER TABLE "public"."rules_profiles" ADD CONSTRAINT "pk_rules_profiles" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table snapshots
-- ----------------------------
CREATE UNIQUE INDEX "analyses_uuid" ON "public"."snapshots" USING btree (
  "uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "snapshot_component" ON "public"."snapshots" USING btree (
  "component_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table snapshots
-- ----------------------------
ALTER TABLE "public"."snapshots" ADD CONSTRAINT "pk_snapshots" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table user_properties
-- ----------------------------
CREATE UNIQUE INDEX "user_properties_user_uuid_kee" ON "public"."user_properties" USING btree (
  "user_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "kee" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table user_properties
-- ----------------------------
ALTER TABLE "public"."user_properties" ADD CONSTRAINT "pk_user_properties" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table user_roles
-- ----------------------------
CREATE INDEX "user_roles_resource" ON "public"."user_roles" USING btree (
  "resource_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);
CREATE INDEX "user_roles_user" ON "public"."user_roles" USING btree (
  "user_id" "pg_catalog"."int4_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table user_roles
-- ----------------------------
ALTER TABLE "public"."user_roles" ADD CONSTRAINT "pk_user_roles" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table user_tokens
-- ----------------------------
CREATE UNIQUE INDEX "user_tokens_token_hash" ON "public"."user_tokens" USING btree (
  "token_hash" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "user_tokens_user_uuid_name" ON "public"."user_tokens" USING btree (
  "user_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "name" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table user_tokens
-- ----------------------------
ALTER TABLE "public"."user_tokens" ADD CONSTRAINT "pk_user_tokens" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table users
-- ----------------------------
CREATE UNIQUE INDEX "uniq_external_id" ON "public"."users" USING btree (
  "external_identity_provider" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "external_id" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "uniq_external_login" ON "public"."users" USING btree (
  "external_identity_provider" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST,
  "external_login" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "users_login" ON "public"."users" USING btree (
  "login" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "users_updated_at" ON "public"."users" USING btree (
  "updated_at" "pg_catalog"."int8_ops" ASC NULLS LAST
);
CREATE UNIQUE INDEX "users_uuid" ON "public"."users" USING btree (
  "uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table users
-- ----------------------------
ALTER TABLE "public"."users" ADD CONSTRAINT "pk_users" PRIMARY KEY ("id");

-- ----------------------------
-- Indexes structure for table webhook_deliveries
-- ----------------------------
CREATE INDEX "ce_task_uuid" ON "public"."webhook_deliveries" USING btree (
  "ce_task_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "component_uuid" ON "public"."webhook_deliveries" USING btree (
  "component_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table webhook_deliveries
-- ----------------------------
ALTER TABLE "public"."webhook_deliveries" ADD CONSTRAINT "pk_webhook_deliveries" PRIMARY KEY ("uuid");

-- ----------------------------
-- Indexes structure for table webhooks
-- ----------------------------
CREATE INDEX "organization_webhook" ON "public"."webhooks" USING btree (
  "organization_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);
CREATE INDEX "project_webhook" ON "public"."webhooks" USING btree (
  "project_uuid" COLLATE "pg_catalog"."default" "pg_catalog"."text_ops" ASC NULLS LAST
);

-- ----------------------------
-- Primary Key structure for table webhooks
-- ----------------------------
ALTER TABLE "public"."webhooks" ADD CONSTRAINT "pk_webhooks" PRIMARY KEY ("uuid");
